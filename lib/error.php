<?php

function handle_messages ($errno, $errstr, $errfile, $errline) {
	global $messages;

	switch ($errno) {
	case E_USER_ERROR:
		$messages['error'][] = $errstr;
		break;
	case E_USER_WARNING:
		$messages['warning'][] = $errstr;
		break;
	case E_USER_NOTICE:
		$messages['notice'][] = $errstr;
		break;
	}

	return true;
}

function message_flash($message) {
    global $messages;
    $messages['info'][] = $message;
}

function message_flash_postponed($message) {
	global $session;
	$session->data['messages']['info'][] = $message;
	$session->save(); 
}


set_error_handler("handle_messages", (E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE));

// empty messages array was causing PHP to spew notices
if (!isset($messages)) $messages = array();



?>
