<?php
require_once("../lib/iCalcreator.class.php");

// disable site template
$no_template = TRUE;

// initialise calendar
$cal = new vcalendar();
$cal->setConfig('unique_id', 'sucs.org');
$cal->setProperty('method', 'PUBLISH');
$cal->setProperty('x-wr-calname', "SUCS Events Calendar");
$cal->setProperty('X-WR-CALDESC', 
		"Upcoming events for members of the Swansea University Computer Society"); 
$cal->setProperty('X-WR-TIMEZONE', 'Europe/London');

//populate with upcoming events
$events = $DB->GetAll("SELECT * FROM events WHERE 
				date_trunc('day', whn) >= date_trunc('day', NOW()) ORDER BY whn ASC");

foreach($events as $event) {
	$vevent = new vevent();
	$vevent->setProperty('dtstart', $event['whn']);
	$vevent->setProperty('LOCATION', $event['location']);
	$vevent->setProperty('summary', $event['name']);
	$vevent->setProperty('description', $event['description']); 
	$vevent->setProperty('categories', $event['category']);
	$cal -> setComponent($vevent);
}

//spit out a shiny new iCal file
$cal->returnCalendar();


?>
