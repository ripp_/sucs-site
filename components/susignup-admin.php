<?php

// gib errars plox
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// include functions and shit we need
include("../lib/member_functions.php");
include("../lib/date.php");
include("../lib/suapiv2.php");

//Restrict access to staff.
$permission="sucsstaff";

// if they have the right privs then lets do shit
if (isset($session->groups[$permission])) {
	//Setup smarty magic, step 1
	$smarty->assign("staff", TRUE);

	// if no mode has been requested then show options
	if(!isset($_REQUEST['mode'])) {
		$mode = 'menu';
	} else {
		$mode = urldecode($_REQUEST['mode']);
	}

	// We have a mode, lets try and work out wtf they want to do

	// they have typed in a sid and want to bootstrap the signup processs
	if ($mode == 'search') {
		if (empty($_REQUEST['sid']) || empty($_REQUEST['snsubmit'])) {
			$mode = 'error';
			$smarty->assign("error_text", "Invalid search request");
		}else{
			$pres=preg_match("/^[0-9]{6}$/",$_REQUEST['sid'],$sid);
			if ($pres!=1) {
				$mode = 'error';
				$smarty->assign("error_text", "Search term doesn't look like a valid student ID");
			} else {
				// they have given us a valid sid lets check to see if they have paid

				// make sure the user/admin/exec isn't an idiot
				// check if they are already signed up and tell them so
				$tmpresult = $sucsDB->Execute("SELECT * FROM members WHERE sid=?", array($_REQUEST['sid']));
				if($tmpresult->fields["sid"] == $sid && $tmpresult->fields["paid"] == paidUntil(time())){
					// let them know they are already signed up and renewed
					message_flash("You are a numpty and have already signed up and paid for this year.");
				// else if check to see if they have signedup and paid for the new year but haven't renewed
				} else if ($tmpresult->fields["sid"] == $sid && $tmpresult->fields["paid"] != paidUntil(time())){
					// renew them!
					renew_membership($tmpresult->fields["username"]);
					// let them know that their account has been renewed
					message_flash("Your SUCS account has been renewed.");
				}else{
					if (check_su_sid($_REQUEST['sid'])) {
						// lets make them a signup slip
						$pass = make_password();
						$query = "INSERT INTO signup (password,sid,issuedby) VALUES ( ?, ?, ?) RETURNING id";
						$attribs[]=$pass;
						$attribs[]=$_REQUEST['sid'];
						$attribs[]='99999'; //SUCS Magic internal use UID

						$id = $sucsDB->Execute($query,$attribs);
						$id = $id->fields['id'];
						if (!$id) {
							$mode="error";
							$smarty->assign("error_text", "An error occurred generating a signup ID. Report the following message to the admins:<br /><pre>".$sucsDB->ErrorMsg()."</pre>");
						} else {
							$smarty->assign('slipid', $id);
							$smarty->assign('slippass', $pass);
							$smarty->assign('sid', $_REQUEST['sid']);
						}
					}else{
						$mode='error';
						$smarty->assign("error_text", "Student does not appear to have paid. Extract fees");
					}
				}
			}
		}
	// mass renewals page
	} else if ($mode == 'renewals') {

			$matches = array();
			$others=0;
			$paidup=0;
			foreach ($just_members as $sumem) {
				$sucsmem = get_sucs_record($sumem['card_number']);
				if ($sucsmem && $sucsmem['paid'] != paidUntil(time()) && $sucsmem['type']==1) {
					$matches[]=array($sumem['purchaser'], $sucsmem['realname'], $sumem['card_number'], $sucsmem['username'], $sucsmem['paid']);
				} else if ($sucsmem && $sucsmem['paid'] == paidUntil(time())) {
					$others++;
					$paidup++;
				} else {
					$others++;
				}
			}
			$smarty->assign("matches", $matches);
			$smarty->assign("others", $others);
			$smarty->assign("paidup", $paidup);
			$smarty->assign("pending", $others - $paidup);
	} else if ($mode == 'renewals2') {
		$failures = array();
		$successes = array();

		if (empty($_REQUEST['renew'])) {
			$mode='error';
			$smarty->assign("error_text", "Can't renew an empty list!");
		} else {
			foreach($_REQUEST['renew'] as $user) {
				if (admin_renew_member($user, $session->username)) {
					$successes[]=$user;
				} else {
					$failures[]=$user;
				}
			}
			$smarty->assign("attempt", count($_REQUEST['renew']));
			$smarty->assign("failures", count($failures));
			$smarty->assign("failusers", $failures);
			$smarty->assign("successes", count($successes));
		}
	} else if ($mode == 'list') {
			$matches = array();
			foreach ($just_members as $sumem) {
				$sucsmem = get_sucs_record($sumem['card_number']);
				if ($sucsmem) {
					$matches[]=array($sumem['purchaser'], $sucsmem['realname'], $sumem['card_number'], $sucsmem['username'], $sucsmem['paid']);
				} else {
					$matches[]=array($sumem['purchaser'], "N/A", $sumem['card_number'], "N/A", "Not signed up");
				}
			}
			function sortbypaid($a, $b) {
				//Lets us array sort by final column ('Paid')
				return ($a[4] < $b[4]) ? -1  : 1;
			}
			usort($matches, 'sortbypaid');
			$smarty->assign("matches", $matches);
	}
$smarty->assign('renewables', get_renewable_members());
}


$smarty->assign('title', 'SU Signup Admin');
$smarty->assign('mode', $mode);
$body = $smarty->fetch("susignup-admin.tpl");
$smarty->assign('body', $body);
$smarty->assign("extra_styles", array("$baseurl/css/susignup-admin.css"));

function get_sucs_record($sid) {
	global $sucsDB;

	$query = "SELECT * FROM members WHERE sid=?;";
	$res  = $sucsDB->Execute($query, array($sid));
	if (!$res || $res->RecordCount()<>1) {
		return FALSE;
	}
        return $res->FetchRow();	
}

function get_renewable_members() {
	global $sucsDB;
	$q = "SELECT username, username||' ('||realname||')' AS display FROM members, member_type WHERE paid != ? AND type=1 AND type=member_type.id ORDER BY paid;";
	$r = $sucsDB->Execute($q, array(paidUntil(time())));
	if(!$r) {
		return FALSE;
	}
	$retvals = array();
	while ($rec=$r->FetchRow()) {
		$retvals[$rec['username']]=$rec['display'];
	}
	return $retvals;
}

function admin_renew_member($renew_name, $admin_name) {
		global $sucsDB;
		renew_member($renew_name);
		$q="SELECT email, typename FROM members, member_type WHERE username=?  AND type=member_type.id;";
		$r=$sucsDB->Execute($q, array($renew_name));
		$message  = "Account Renewal notification\n\n";
		$message .= "Account   : ".$renew_name."\n";
		$message .= "User Type : ".$r->fields['typename']."\n";
		$message .= "Renewed by: ".$admin_name."\n\n";
		$message .= "**** Payment was made via the SU payments system ****\n";
		$message .= "Regards\n  The SU Renewals script";
		mail("exec@sucs.org","Account Renewal",$message);
		return TRUE;
}

