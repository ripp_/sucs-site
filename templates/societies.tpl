<p>The Student Union's societies website can be found at <a href="http://swansea-union.co.uk/societies">http://swansea-union.co.uk/societies</a>.</p>

<p>The following societies' websites are hosted by SUCS:</p>

<ul>
{foreach name=societies from=$societies item=society}
<li><a href="https://sucs.org/~{$society.username|escape:'url'}">{$society.fullname|escape:'htmlall'}</a>{if $society.descr} - {$society.descr}{/if}</li>
{/foreach}
</ul>
