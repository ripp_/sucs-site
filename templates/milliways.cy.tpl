<h2>Defnyddwyr Milliways Ar Hyn O Bryd</h2>
{if $people|@count < 1}
<p>No-one logged on to Milliways</p>
{else}
<table class="border">
    <tr>
	<th>Ffugenw</th>
	<th>Amser Diogi</th>
	<th>What</th>
    </tr>
    {foreach name=people from=$people key=personnum item=person}
    <tr>
	<td>{$person.username}</td>
	<td>{$person.idle}</td>
	<td>{$person.what}</td>
    </tr>
    {/foreach}
</table>
{/if}
