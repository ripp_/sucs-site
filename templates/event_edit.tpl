<form class="admin" method="post" action="{$baseurl}{$path}">
<input type="hidden" name="action" value="save" />
<input type="hidden" name="id" value="{$event.id}" />
<div class="row">
	<label for="name">Event Name</label>
	<span class="textinput"><input type="text" name="name" {if $event.name}value="{$event.name}"{/if} /></span>
</div>
<div class="row">
	<label>Date</label>
	<span class="textinput">{html_select_date end_year='+2' }</span>
</div>
<div class="row">
	<label>Time</label>
	<span class="textinput">{html_select_time display_seconds=false minute_interval=5}</span>
</div>
<div class="row">
	<label for="category">Event Type</label>
	<span class="textinput">
		{foreach from=$event_categories item=category}
		<input type="radio" name="category" value="{$category}" {if $category eq $event.category}checked="checked"{/if} /> {$category}<br />
		{/foreach}
	</span>
</div>
<div class="row">
	<label for="location">Location</label>
	<span class="textinput"><input type="text" name="location" {if $event.location}value="{$event.location}"{/if} /></span>
</div>
<div class="row">
	<label for="description">Description</label>
	<span class="textinput"><textarea name="description" cols="50" rows="15">{if $event.description}{$event.description}{/if}</textarea></span>
</div>
<div class="row">
<input type="submit" value="Save" />
</div>

</form>