<h3>Projects currently hosted by SUCS</h3>

<p>SUCS has recently deployed <a href="https://projects.sucs.org/explore">Gitlab</a> with a more expansive list of projects.</p>

<dl>
{foreach name=projects from=$projects item=project}
<dt><a href="http://projects.sucs.org/projects/{$project.filename}">{$project.name}</a></dt>
{if $project.descr}<dd>{$project.descr}{/if}</dd>
{/foreach}
</dl>