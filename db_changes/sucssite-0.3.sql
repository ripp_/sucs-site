DELETE FROM session;
ALTER TABLE session DROP COLUMN time;
ALTER TABLE session ADD COLUMN logintime timestamp(0) without time zone;
ALTER TABLE session ADD COLUMN lastseen timestamp(0) without time zone;
DELETE FROM menu WHERE title='Bananas';
