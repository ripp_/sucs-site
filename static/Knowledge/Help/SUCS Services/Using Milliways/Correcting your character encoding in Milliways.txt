<p>Sometimes someone in Milliways will type something that your terminal emulator can&#39;t cope with. This is usually caused by your terminal emulator not being set to use the same character set as the Milliways program.</p>

<p><a href="../Logging%20in%20remotely/Configuring%20PuTTY">PuTTY</a>&#39;s default character encoding is <a href="http://en.wikipedia.org/wiki/ISO/IEC_8859-1">ISO 8859-1</a>, also known as Latin1. This is a Western European character encoding containing characters for e.g. French and German, as well as the pound sign. Milliways, on the other hand (or more specifically your shell session), by default uses the <a href="http://en.wikipedia.org/wiki/UTF-8">UTF-8</a> character encoding, which is an encoding of <a href="http://www.unicode.org/">Unicode</a>, the unified character set. Milliways quite frequently encounters the pound sign, and this will get screwed up if your terminal receives it as UTF-8 but interprets it as Latin1. You will also simply lose any characters in UTF-8 which can&#39;t be translated into Latin1.</p>

<p>To fix this in PuTTY, open the window menu (which has some extra entries) and select &#39;Change settings...&#39;:</p>
<img src="/pictures/mw.png" alt="PuTTY window menu" width="530" height="362" />

<p>This brings up a dialogue box similar to the one you get when you first run PuTTY (sans options that can only be changed before the connection is made). Select &#39;Translation&#39; and pick &#39;UTF-8&#39; from the combo box labelled &#39;Received data assumed to be in which character set&#39;:</p>
<img src="/pictures/mwconf.png" alt="PuTTY configuration dialogue box" width="373" height="359" />

<p>Now click &#39;Apply&#39;, and any more weird characters should be displayed correctly (assuming you have appropriate fonts installed).</p>