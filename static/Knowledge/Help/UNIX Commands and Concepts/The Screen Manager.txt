<p>There are a number of circumstances under which it is useful to be able to move the same session between computers or leave commands running in the background then come back to them later. <tt>screen</tt> is ideal for this.</p>

<p>The command is initially invoked with the following command:</p>

<pre class="console">$ screen</pre>

<p>This will start a new <tt>screen</tt> session in which you can do anything you can do in a normal session. The only difference is that it takes over <i>Ctrl-a</i> as an escape character to run its own commands. If you want to use <i>Ctrl-a</i> (Home) normally, you will need to press <i>Ctrl-a a</i>. Help is available with the key-combination <i>Ctrl-a ?</i>.</p>

<p>You can also make <tt>screen</tt> run a command directly, without going to the shell. For example, to start <a href="/wiki/milliways">Milliways</a>, you would type:</p>

<pre class="console">$ screen mw</pre>

<p>Once you are running a command, you can detatch the session with <i>Ctrl-a d</i>. If you want to end the session that you opened <tt>screen</tt> from as well, use <i>Ctrl-a DD</i>.</p>

<p>When you want to reattach the session, you can use:</p>

<pre class="console">$ screen -r</pre>

<p>If you left the screen attached and want to close that terminal and reconnect it in your current session, use:</p>

<pre class="console">$ screen -Dr</pre>

<p>You can also reattach the session without closing it elsewhere. The command for this is:</p>

<pre class="console">$ screen -x</pre>