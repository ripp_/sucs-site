<h2>Donations Policy</h2>
<ol>
<li> All donations to the society are final, and non-returnable. Any equipment loaned to the society must have the terms of the loan (e.g. time period and/or conditions of fair use) laid out clearly before the loan is accepted.</li>
<li>The society shall not feel obligated to reimburse a donator in any way. Donations should be given freely for the benefit of the society and its members, not for personal gain.</li>
<li>Only equipment that has a direct and immediate use, or a clearly defined planned near-future use, shall be accepted. We simply do not have the storage capacity to collect items that 'may be useful someday'.</li>
<li>A clear log shall be kept of who, what and when equipment was donated, and kept in a prominent place so as to honour our benefactors.</li>
<li> The Society can not be held responsible for the safe keeping of any items loaned or otherwise to it. Whilst every effort will be maintained to protect it, any subsequent damage or theft is entirely at your own risk.</li>
<li> The Society retains the right to return or dispose of any item that threatens, or is believed to threaten the safety rules laid down by the Student Union and the University.</li>
<li> The Society may use, or dispose of donated equipment (non-loan items) in any manner that benefits the society, this includes potentially selling items in order to raise funds to purchase more directly useful items, or donating or swapping equipment with others for good will and favour towards the society.</li>
</ol>
<h2>Donated Equipment List</h2>
<p>  This list is incomplete</p>
<table border="0" class="border wide">
<tbody>
<tr>
<th>Who</th><th>Date</th><th>Type</th><th>Items</th>
</tr>
<tr>
<td>Arthur</td>
<td>Sept 99</td>
<td>Loan</td>
<td>Deskjet 540 colour printer</td>
</tr>
<tr>
<td>Zaphod &amp; Spook</td>
<td>2000</td>
<td>Loan</td>
<td>Sun Workstations</td>
</tr>
<tr>
<td>Anarchy</td>
<td>2000</td>
<td>Loan</td>
<td>Motherboard, CPU, and RAM for Platinum</td>
</tr>
<tr>
<td>Milamber</td>
<td>2001</td>
<td>Loan</td>
<td>Brother Mono Laser Printer.</td>
</tr>
<tr>
<td>Weazel</td>
<td>Sep 2002</td>
<td>Loan</td>
<td>120Gb IDE Hard disk for Silver</td>
</tr>
<tr>
<td>Rohan</td>
<td>Jan 2003</td>
<td>Donated</td>
<td>PII 350MHz machine to be used as Cobalt<br />      Dual CPU machine<br />      Empty 1U Rack Case </td>
</tr>
<tr>
<td>Anarchy</td>
<td>Feb 2003</td>
<td>Donated</td>
<td>17" Dell Monitor for Cobalt</td>
</tr>
<tr>
<td>Anarchy &amp; Dick</td>
<td>Feb 2003</td>
<td>Donated</td>
<td>Profits from 2002 BT lecture, presented as components to build 2 new workstations</td>
</tr>
<tr>
<td>Anarchy</td>
<td>Dec 2003</td>
<td>Loan</td>
<td>Pentium to replace the router, a Cyrix machine for workstation</td>
</tr>
<tr>
<td>jk</td>
<td>June 2005</td>
<td>Donated</td>
<td>PIII 1GHz + Board + Memory, used to create thorium</td>
</tr>
<tr>
<td>aeternus</td>
<td>Nov 2005</td>
<td>Donated</td>
<td>19" monitor. Used to replace platinum's old small one</td>
</tr>
<tr>
<td>Rohan</td>
<td>Nov 2005</td>
<td>Donated</td>
<td>Dual PIII 1GHz machine with SCSI disk<br />      2 other boxes (expand on this..)</td>
</tr>
<tr>
<td>EMC via Rohan</td>
<td>Nov 2005</td>
<td>Donated</td>
<td>PIII 800MHz 1U box with 4x80GB disks</td>
</tr>
<tr>
<td>gimbo</td>
<td>Feb 2006</td>
<td>Donated</td>
<td>17" monitor. Used to replace zinc's old one.</td>
</tr>
<tr>
<td>mattaw</td>
<td>Sep 2006</td>
<td>Donated</td>
<td>Athlon PC</td>
</tr>
<tr>
<td>Rohan<br /></td>
<td>Summer 2006<br /></td>
<td>Donated<br /></td>
<td>Another Dual PIII 1GHz machine with SCSI disk</td>
</tr>
<tr>
<td>Rohan<br /></td>
<td>August 2011<br /></td>
<td>Loan<br /></td>
<td>
<p>UPS (Smart-UPS 1400) and Switch (3Com 3870)</p>
</td>
</tr>
<tr>
<td>Problaze<br /></td>
<td>September 2012<br /></td>
<td>Donated<br /></td>
<td>
<p>Dell Poweredge 1950</p>
</td>
</tr>
</tbody>
</table>