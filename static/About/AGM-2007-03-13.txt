<h1 id="sucs_agm_minutes">SUCS AGM Minutes</h1>

<p><em>Date of AGM: 2007-03-13, 6pm</em></p>

<h2 id="in_attendance">In attendance</h2>

<p>Michael Howard, William Blackstock, Sitsofte Wheeler, Kate Locke, Chris Melvin, Ian Tucker, Sean Handley, Steve Welti, Andy Price, Chris McKenna, Graham Cole, Dave Arter, Darius Garnham, Nick Corlett, Tom Bradley, Richard Byrne, Chris Jones, Nick Shepard, James Frost, Chris Elsmore, Kat Meyrick, Kristian Knevitt, James Olver.</p>

<p>Mini eggs were handed out.</p>

<hr />

<h2 id="review_of_last_year">Review of last year</h2>

<ul>
<li>Logo and website re-done, which was successful. Possible small projects remain for website - see Graham.</li>
<li>Gameserver - not as great as it should be. Focused on it for Freshers&rsquo; Fayre but didn&rsquo;t follow through. Work to improve it this year - look into working properly on campus. Gaming on Sunday nights has been successful. </li>
<li>Silver update went well - now fully operational.</li>
<li>Freshers&rsquo; Fayre last year - lots of new people, got their money but have hardly seen them (if at all). Focus efforts on more active members this year? RFID issues noted.</li>

<li>Any questions of last year?
<ul>
<li>Andy, how much money did we have? &pound;466 now, hasn&#39;t changed from oct. </li>
<li>Wedge: more members - did we not see people because we got them to join over superficial things? More on this later on.</li>
</ul></li>
</ul>

<h2 id="silver_upgrade">Silver upgrade</h2>

<ul>
<li>Fedora has short support lifetime. Possible move to Centos or Debian. Find interest and discuss with admin.</li>
</ul>

<h2 id="budget">Budget</h2>

<ul>
<li>Discussion of TFTs. Possible now they have come down in price.</li>
<li>Mac Mini - not many Macs on campus. Apple say educational discount is available. Ask about Apple sponsorship. Could ask SU for Mac Mini? Talk of lots of people wanting to use it - not enough with only one? Silly buying more machines when possibly moving to a smaller room. Buy just one, and it could always replace a bigger machine if/when we move.</li>
<li>Also sponsorship from Ubuntu etc, or just get free stuff for Freshers&rsquo;.</li>
<li>Upgrades - Upgrades to workstations would require new mobo, RAM and CPU.</li>
<li>Chris Melvin offers a GeForce 4.</li>
<li>Price up Dells etc. Ask for education discounts.</li>

<li>Budget for end of exams party </li>
<li>Card Readers &amp; bluetooth dongles - Two 3 1/2&rdquo; bay card readers for workstations. Mac Mini has Bluetooth :) Card reader needs USB2 to be useful- only Gameserver and Old Silver have that currently. No point buying USB2 cards if upgrading workstations. Bluetooth probably wouldn&rsquo;t be used, so won&rsquo;t be bought.</li>
<li>LAN events could be used to raise money by charging at them.</li>
<li>Tutorials could be charged? Not really enough demand, people wouldn&rsquo;t pay. Same for building PCs for people, etc - SUCS would get blamed if anything went wrong. Don&rsquo;t need the hassle.</li>
</ul>

<h2 id="alumni">Alumni</h2>

<ul>
<li>Alumni status has been set up. &pound;20 for Alumni. </li>
<li>Put it in the constitution. &ldquo;Alumni membership open to any previous member of the SU, at the current executive committees&rsquo; discretion.&rdquo; </li>
<li>Vote: 19 for, none against, no abstentions. Unanimous. Motion passed.</li>
</ul>

<h2 id="hoodies">Hoodies</h2>

<ul>
<li>Dicussion on styles. 
<ul>
<li>Black, White, Bright Red, Dark Red, Navy, Grey possible. </li>
<li>Vote. Black most popular, second is navy.</li>
</ul></li>
</ul>

<h2 id="room_move">Room Move</h2>

<ul>

<li>Not happening right now. Sort out what we need from a new room. Send to Owen. </li>
<li>See Estates.  Organise meeting between SU treasurer, Estates and SUCS Committee.</li>
<li>Note they have said room will be 24hr access.</li>
<li>Do logs for room usage to prove how busy we are.</li>
<li>Get inside and measure new room up.</li>
<li>Do we want shelves in the new room?  Not if they decrease amount of workstation area.</li>
</ul>

<h2 id="freshers8217_fayre">Freshers&rsquo; Fayre</h2>

<ul>
<li>Last year successful. Problems getting people involved and prospective people for the new committee.</li>
<li>Get people to know if LIS are planning another change in card system - e-mail people before the Fayre. </li>
<li>Loads of people standing in front of the stand is not a good look! </li>
<li>Get accounts and sign up people straight away?</li>
<li>Mobile admins with signup slips could sign people whenever, all over campus.</li>
<li>Push additional skills SUCS offers - ability to admin networks, etc. We currently don&rsquo;t advertise these things at all. Push the skillset you get - SUCS needs fresh people :) </li>
<li>Working with CompSci for PC software updates, etc? (helping them with their updates)</li>

<li>Houseparty for freshers - more socials, talks etc. People should be in the room whilst Freshers&rsquo; Fayre is on - nice friendly people :)</li>
<li>Clean room before Freshers&rsquo;. Get free stuff for freshers from Ubuntu, etc. </li>
<li>Get events for freshers planned beforehand (socials, Que Pasa, Talk things, etc).</li>
</ul>

<h2 id="lan_gaming">LAN Gaming</h2>

<ul>
<li>Get closer to other societies for more LAN event advertising, create close ties with other societies. </li>

<li>Discussion of the LAN society situation. Do we want to take on what they did?</li>
<li>Decision that SUCS has trouble keeping itself going as it is, let alone stretching itself to games things too.</li>
<li>May help start up the LAN/gaming soc again, but then let it continue by itself. Can team up with SUCS for events, and maybe offer a discount to people who&rsquo;re members of both socs.</li>
</ul>

<h2 id="any_other_business">Any Other Business</h2>

<ul>
<li>Socials - is Friday a good day / 1pm a good time?
<ul>
<li>Leave it as it is until JCs gets totally refitted and smoke free, and then come back to problem.</li>

</ul></li>
<li>Kat: Windows machines? SUCS being closer to the CS dept, possibilty of SUCS working with CS for better facilities. CS not happy about students messing with admin things, etc. Adminning our own stuff is more important - not really enough people to do that at the moment.</li>
<li>Ian: SUCS should really push real world adminning skills you can get. 
<ul>
<li>Sponsorship from other people for the society?</li>
</ul></li>
<li>MW - New people to have script privs, no timeouts, etc, out of the box. Maybe part of the user add script. </li>
</ul>

<p><em>AGM finished</em></p>
