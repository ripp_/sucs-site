<?

function msg_tx($text, $time, $fg, $bg)
{
	$f = fopen("http://door.sucs.org/sendstatus.py?text=".urlencode($text)."&amp;timeout=$time&amp;fg=$fg&amp;bg=$bg", "r");
	fclose($f);
}

function msg_good($text, $time=5)
{
	msg_tx($text, $time, "0,0,0", "128,255,128");
}

function msg_bad($text, $time=3)
{
	msg_tx($text, $time, "0,0,0", "255,64,64");
}

header("Content-type: text/plain");

// Initialise the database
require("/usr/share/adodb/adodb.inc.php");
$DB = NewADOConnection('postgres8');
if ($DB->Connect('dbname=sucs') === FALSE) {
	echo "DB fail\n";
	exit;
}
$DB->SetFetchMode(ADODB_FETCH_ASSOC);


	$f = fopen("/tmp/doorlog", "a");

if (isset($_REQUEST['code'])) {

	// Expire old requests
	$DB->Execute("delete from doorknock where start < NOW() - 30 seconds:reltime"); 

	// Parse out the string
	$code = $_REQUEST['code'];

	$codes = split(' ',$code);
	if ($codes[0] != "SUCS") {
		echo "Unrecognised codeword '{$codes[0]}'\n";
		msg_bad("Invalid slip");
		exit;
	}

	// look up the user
	$user = $DB->GetAll("select * from signup where id=?",array($codes[1]));
	if (!is_array($user) || count($user) < 1) {
		echo "Unrecognised signup id\n";
		msg_bad("Invalid slip");
		exit;
	}

	if ($codes[2] != $user[0]['password']) {
		echo "Password mismatch\n";
		msg_bad("Invalid slip");
		exit;
	}

	if ($user[0]['card'] != "") {
		echo "User already has a card\n";
		msg_bad("Slip already used");
		exit;
	}

	// all looks valid so far
	// check we arent bouncing ourselves out

	$exist = $DB->GetAll("select * from doorknock");
	if (is_array($exist) && count($exist) > 0) {
		print_r($exist);
		if ($exist[0]['suid'] == $user[0]['id']) {
			echo "Already in progress, no action\n";
			exit;
		}

		$DB->Execute("delete from doorknock");
		echo "Bouncing out signup id={$exist[0]['suid']}\n";
	}
	$DB->Execute("insert into doorknock (suid,start) values(?,NOW())", array($user[0]['id']));
	echo "Start waiting for id={$user[0]['id']}\n";
	msg_good("Please swipe ID Card to complete registration", 10);
	exit;



} else
// a card was swiped at the door, try to match it
if (isset($_REQUEST['id'])) {
	$card = $_REQUEST['id'];

	$exist = $DB->GetAll("select * from doorknock");
	if (!is_array($exist) || count($exist) < 1) {
		echo "No registration in progress, ignoring.\n";
		exit;
	}
	
	$signup = $DB->GetAll("select * from signup where id=?",array($exist[0]['suid']));
	if (!is_array($signup) || count($signup) < 1) {
		echo "Unrecognised signup id\n";
		msg_bad("Invalid slip");
		$DB->Execute("delete from doorknock");
		exit;
	}

	// should really check the card isnt already registered first
	$DB->Execute("update signup set card=? where id=?", array($card, $signup[0]['id']));
	echo "Registering card '$card' to signup id {$signup[0]['id']}\n";
	$DB->Execute("delete from doorknock");

	// User is registered, stick it in the real doorcards list too
	if ($signup[0]['username'] != "") {
		$user = $DB->GetAll("select * from members where username=?", array($signup[0]['username']));
		if (!is_array($user) || count($user) < 1) {
			echo "Cant find username '{$signup[0]['username']}' to full reg the card\n";
		} else {
			$DB->Execute("insert into doorcards (uid, cardnumber) values (?,?)", array($user[0]['uid'], $card));
			echo "Registering card to uid={$user[0]['uid']} username {$user[0]['username']}\n";
		}
	}
	unlink("/tmp/cards");
	system("/usr/local/bin/db-to-cards.php");
	system("sudo /usr/local/bin/putcardsfile");
	unlink("/tmp/cards");
	msg_good("Card now registered, swipe again for access");

	exit;
}

?>
